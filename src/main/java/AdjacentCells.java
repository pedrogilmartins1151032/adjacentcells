import IO.IOMatrixFile;
import Model.IslandList;

import javax.swing.*;
import javax.swing.filechooser.FileSystemView;
import java.io.File;

public class AdjacentCells {

    public static void main(String[] args) {

        // *********** Choosing directory ************
        JFileChooser fileChooser = new JFileChooser(FileSystemView.getFileSystemView().getDefaultDirectory());
        int returnValue = fileChooser.showOpenDialog(null);
        if (returnValue == JFileChooser.CANCEL_OPTION) {
            return;
        }
        File file = fileChooser.getSelectedFile();

        // *********** Reading ************
        long startTime = System.currentTimeMillis();

        boolean[][] m = IOMatrixFile.IOReadFile(file);

        long endTime = System.currentTimeMillis();
        double elapsedTimeR = (endTime - startTime) / 1000f;

        System.out.printf("Tempo a ler: %.3f segundos\n", elapsedTimeR);


        // *********** Processing ************
        startTime = System.currentTimeMillis();

        IOMatrixFile.setUpResultFile();
        IOMatrixFile.openBufferedWriter();

        IslandList li = new IslandList();
        li.findIslands(m);

        IOMatrixFile.closeBufferedWriter();

        endTime = System.currentTimeMillis();
        double elapsedTimeP = (endTime - startTime) / 1000f;

        System.out.printf("Tempo a processar: %.3f segundos\n", elapsedTimeP);
        System.out.printf("Tempo total: %.3f segundos\n", elapsedTimeR + elapsedTimeP);
    }
}
