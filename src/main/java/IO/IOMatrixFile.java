package IO;

import Model.Island;

import java.io.*;
import java.util.LinkedList;
import java.util.List;


public class IOMatrixFile {

    private static List<boolean[]> m;
    private static int lineLength;
    private static int dimension;

    private static File file;
    private static BufferedWriter out;

    public IOMatrixFile() {
    }


    public static boolean[][] IOReadFile(File f) {

        m = new LinkedList<>();
        String line;
        try {
            FileReader frd = new FileReader(f);
            BufferedReader brd = new BufferedReader(frd, 2 * 1048576);

            brd.readLine();
            sizes(brd.readLine());

            while ((line = brd.readLine()) != null) {
                parseLine(line);
            }
            brd.close();
            frd.close();
        } catch (IOException | ArrayIndexOutOfBoundsException exc) {
            //System.out.println("Erro 1: " + exc.getMessage());
        }

        return m.toArray(new boolean[m.size()][]);
    }


    /**
     * @param line Line to use as dimension pattern
     */
    private static void sizes(String line) {

        lineLength = line.length();
        dimension = (lineLength - 2) / 2;
        parseLine(line);
    }

    /**
     */
    private static void parseLine(String line) {

        int index = 0, i;
        try {
            boolean[] mAux = new boolean[dimension];
            for (i = 1; i < lineLength - 1; i += 2) {
                mAux[index] = line.charAt(i) == 49;
                index++;
            }
            m.add(mAux);
        } catch (IndexOutOfBoundsException exc) {
            //System.out.println("Erro 2: " + exc.getMessage());
        }
    }

    /**
     *
     */
    public static void setUpResultFile() {

        String workingDir = System.getProperty("user.dir");
        file = new File(workingDir + System.getProperty("file.separator") + "resultados.txt");
        if (file.exists()) {
            file.delete();
        }
    }


    /**
     * @return BufferedWriter
     */
    public static BufferedWriter openBufferedWriter() {
        out = null;
        try {
            out = new BufferedWriter(new FileWriter(file, true), 2 * 1048576);
        } catch (IOException e) {
            //e.printStackTrace();
        }
        return out;
    }

    public static void closeBufferedWriter() {
        try {
            out.close();
        } catch (IOException e) {
            //e.printStackTrace();
        }
    }


    public static void toFile(Island island) {
        try {
            out.append(island.toString());
            out.newLine();
        } catch (IOException e) {
            //e.printStackTrace();
        }
    }
}
