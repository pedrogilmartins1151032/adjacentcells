package Model;

import IO.IOMatrixFile;

public class IslandList {

    private int mLines;
    private int mColumns;

    public IslandList() {
    }

    public void findIslands(boolean[][] m) {

        this.mLines = m.length;
        this.mColumns = m[0].length;

        processeMatrix(m);
    }

    /**
     *
     * @param mat Working matrix
     */
    public void processeMatrix(boolean[][] mat) {

        for (short line = 0; line < mLines; line++) {
            for (short column = 0; column < mColumns; column++) {

                if (mat[line][column]) {
                    mat[line][column] = false;
                    new Island(line, column, mat);
                }
            }
        }
    }
}
