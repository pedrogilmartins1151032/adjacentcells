package Model;

public class Cell {
    private short row;
    private short column;


    public Cell(short row, short column) {
        this.row = row;
        this.column = column;
    }

    public short getRow() {
        return row;
    }

    public short getColumn() {
        return column;
    }

    @Override
    public String toString() {
        return "[" + this.row + ',' + this.column + ']';
    }
}
