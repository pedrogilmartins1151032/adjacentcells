package Model;

import IO.IOMatrixFile;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

/**
 *
 */
public class Island {

    private final List<Cell> cellList;
    private short size = 0;
    private final boolean[][] m;

    /**
     * @param line Line position
     * @param column Column position
     * @param mat Matrix of values
     */
    public Island(short line, short column, boolean[][] mat) {

        this.m = mat;
        this.cellList = new LinkedList<>();
        findCells(line, column);
    }

    /**
     * @param line Line position
     * @param column Column position
     */
    public void findCells(short line, short column) {

        List<Cell> queue = new LinkedList<>();

        Cell cell = new Cell(line, column);
        this.cellList.add(cell);
        size++;
        queue.add(cell);
        findNeighbors(queue);

        if (this.size > 1) {
            IOMatrixFile.toFile(this);
        }
    }

    /**
     * @param queue Queue to evaluate
     */
    public void findNeighbors(List<Cell> queue) {
        Cell cell;
        while (!queue.isEmpty()) {
            cell = queue.remove(0);
            short l = cell.getRow();
            short c = cell.getColumn();
            evaluateNeighbor(queue, l, (short) (c + 1));
            evaluateNeighbor(queue, l, (short) (c - 1));
            evaluateNeighbor(queue, (short) (l + 1), c);
            evaluateNeighbor(queue, (short) (l - 1), c);
            findNeighbors(queue);
        }
    }

    /**
     * @param queue Queue to evaluate
     * @param l line position
     * @param c column position
     */
    public void evaluateNeighbor(List<Cell> queue, short l, short c) {

        try {
            if (m[l][c]) {
                m[l][c] = false;
                Cell cell = new Cell(l, c);
                this.cellList.add(cell);
                size++;
                queue.add(cell);
            }
        } catch (ArrayIndexOutOfBoundsException e) {
            //System.out.println("ERROR ArrayIndexOutOfBounds");
        }
    }


    /**
     * @return Island representation
     */
    @Override
    public String toString() {

        StringBuilder sb = new StringBuilder("[");
        Iterator<Cell> it = this.cellList.listIterator();
        for (int i = 0; i < this.size - 1; i++) {
            sb.append(it.next().toString());
            sb.append(", ");
        }
        sb.append(it.next().toString());
        sb.append("]");
        return sb.toString();
    }


}
